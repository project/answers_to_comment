README Answers to comment module
--------------------------------------

 * Introduction

  With this module you can:
  - Transform an answer of a user in a comment .
  - Choose if you assign the comment to the question or another answer.
  - Transform a comment of a user in an answer .
  - Very useful for moderation (you have the permission 'move comment to answer'
  & 'move answer to comment' that you can assign to roles)

 * Requirements

  You need the answer module with a version of the branch 4.X:
  https://www.drupal.org/project/answers
  And the module entity: https://www.drupal.org/project/entity

  * Installation

  Just go in the modules section and enable it.

  * Configuration

  To authorize people to transform answers to comment and the reverse, just give
  the permissions 'move comment to answer' and 'move answer to comment' to roles
  you wants to.
