<?php
/**
 * @file
 * Answers to comment callback functions.
 */

/**
 * Transforms an answer to a comment.
 *
 * @param int $nid
 *   The node Id.
 */
function answers_to_comment_to_comment($nid) {

  $answer = node_load($nid);
  // Get the related question.
  $related_question = field_get_items('node', $answer, 'answers_related_question');
  $related_question = reset($related_question);

  // Check if there is others published answers.
  $query = db_select('node', 'n')
    ->addtag('node_access');
  $query->join('field_data_answers_related_question', 'q', 'n.nid = q.entity_id');
  $query->join('field_data_body', 'b', 'b.entity_id = q.entity_id');
  $query->join('users', 'u', 'n.uid = u.uid');
  $drupal_results = $query->fields('n', array('nid', 'title', 'uid'))
    ->fields('b', array('body_value'))
    ->fields('u', array('name'))
    ->condition('q.answers_related_question_target_id', $related_question['target_id'], '=')
    ->condition('n.status', NODE_PUBLISHED, '=')
    ->execute();
  // If just one result is that we have to put on question.
  if ($drupal_results->rowCount() == 1) {
    answers_to_comment_transform_to_comment($answer, $related_question['target_id']);
    drupal_set_message(t('Answer has been transformed to a comment!'));
    drupal_goto('node/' . $related_question['target_id']);
  }
  elseif ($drupal_results->rowCount() > 1) {
    // You have to choose the answer (without the current of course)
    // or the question.
    $targets[$related_question['target_id']] = node_load($related_question['target_id']);
    while ($record_drupal = $drupal_results->fetchAssoc()) {
      if ($record_drupal['nid'] != $nid) {
        $targets[$record_drupal['nid']] = $record_drupal;
      }
    }
    return drupal_get_form('answers_to_comment_choose_node_form', array('source_nid' => $nid, 'targets' => $targets));
  }
}

/**
 * Transformer a comment to an answer.
 *
 * @param int $cid
 *   The comment cid.
 */
function answers_to_comment_to_answer($cid) {
  $comment_wrapper = entity_metadata_wrapper('comment', comment_load($cid));
  if (!empty($comment_wrapper)) {
    // Load the node associated to the comment.
    $node = node_load($comment_wrapper->node->value()->nid);
    // Is it an answer ?.
    if ($node->type == 'answers_answer') {
      // Load the parent who is the question.
      $related_question = reset(field_get_items('node', $node, 'answers_related_question'));
      $question = node_load($related_question['target_id']);
    }
    else {
      // It's the question.
      $question = $node;
    }
    // Prepare the creation of the answer with entity.
    $answer_entity = entity_create('node', array('type' => 'answers_answer'));
    // Tell others modules that we will transform.
    $answer_entity->transform_to_answer = TRUE;
    $answer_wrapper = entity_metadata_wrapper('node', $answer_entity);
    $answer_wrapper->answers_related_question->set(intval($question->nid));
    $answer_wrapper->author = $comment_wrapper->author->value();
    $answer_wrapper->created = $comment_wrapper->created->value();
    $answer_wrapper->title = $comment_wrapper->subject->value();
    $answer_wrapper->body->set(array('value' => $comment_wrapper->comment_body->value->raw()));

    // Let's see if any other module wants to do stuff
    // before we process the transformation.
    module_invoke_all('answers_to_comment_preprocess', $comment_wrapper, $answer_wrapper);

    $answer_wrapper->save();

    // Let's see if any other module wants to do stuff before we delete
    // the source.
    module_invoke_all('answers_to_comment_done', $comment_wrapper, $answer_wrapper);

    // Delete the original comment.
    $comment_wrapper->delete();
    drupal_set_message(t('Comment <b>!title</b> has been transformed to an answer!', array('!title' => $answer_wrapper->title->value())));
    drupal_goto('node/' . $question->nid);
  }
}
