<?php

/**
 * @file
 * Answers to comment API.
 */

/**
 * Hook to let other modules do stuff before $new is saved.
 *
 * @param object $source_entity
 *   The original entity.
 * @param object $new_entity
 *   The new entity.
 */
function hook_answers_to_comment_preprocess($source_entity, $new_entity) {

}

/**
 * Hook to let other modules do stuff when something is transformed.
 *
 * @param object $source_entity
 *   The original entity.
 * @param object $new_entity
 *   The new entity.
 */
function hook_answers_to_comment_done($source_entity, $new_entity) {

}
